var express = require('express');
var app = express();
var path = require("path");

app.use(express.static('./'));

app.listen(80, function () {
  console.log("Listening on 80...");
});
