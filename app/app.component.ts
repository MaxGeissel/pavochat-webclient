import {Component} from "@angular/core";
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from "@angular/router-deprecated"
import {NavigationComponent} from "./templates/navigation/navigation.component";
import {LoginViewComponent} from "./templates/loginview/loginview.component";
import {ChatViewComponent} from "./templates/chatview/chatview.component";
import {ChatServerConnection} from "./templates/chatserverconnection/chatserverconnection";

@Component({
    selector: "my-app",
    templateUrl: "app/app.html",
    directives: [NavigationComponent, ROUTER_DIRECTIVES],
    providers: [ChatServerConnection]
})
@RouteConfig([
  {path: '/', name: 'Login', component: LoginViewComponent, useAsDefault: true},
  {path: '/chatview', name: 'ChatView', component: ChatViewComponent}
])
export class AppComponent { }
