import { Injectable } from '@angular/core';
import {Message} from '../../templates/Message';

@Injectable()
export class MediaParserService {
  public resolveLinksAndMedia(text: string): string {
    const regExpImageLinkOnly: RegExp = /(http[s]?:\/\/\S*\.)(gif|png|jpg|jpeg)/g;
    const regExpVideoLinkOnly: RegExp = /(http[s]?:\/\/\S*\.)(mp4|mpg|mpeg|vob|m2p|ts|mov|avi|wmv|asf|mkv|webm|flv|3gp)/g;
    const regExpYoutubeEmbedLinkOnly: RegExp = /(http[s]?:\/\/\S*\.)(com|de|be)(\W)(embed)(\W)(\S*)/g;
    const regExpYoutubeWatchLinkOnly: RegExp = /(http[s]?:\/\/\S*\.)(com|de|be)(\W)(watch\Wv)(\W)(\S*)/g;
    const regExpYoutubeShareLinkOnly: RegExp = /(http[s]?:\/\/youtu\.)(com|de|be)(\W)(\S*)/g;
    const regExpAnyLink: RegExp = /(http[s]?:\/\/\S*)/g;
    const regExpLinkOnly: RegExp = /(^(?!src=")http[s]?:\/\/\S*)/g;

    const patterns = {
      smile: /:-?\)/gm,
      sad: /:-?\(/gm,
      wink: /;-?\)/gm,
      plain: /:-?\|/gm,
      grin: /:-?D/gm,
      surprise: /:-?O/gm,
      tongue: /:-?P/gm,
      dazed: /%-?\)/gm,
    };

    const emoticons = {
      smile: '<img src="app/img/27.gif" alt=":-)" />',
      sad: '<img src="app/img/43.gif" alt=":-(" />',
      plain: '<img src="app/img/5.gif" alt=":-|" />',
      grin: '<img src="app/img/3.gif" alt=":-D" />',
      surprise: '<img src="app/img/21.gif" alt=":-O" />',
      dazed: '<img src="app/img/54.gif" alt="%-)" />',
    };


    // escape html
    text = text.replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");

    text = text.replace(patterns.sad, emoticons.sad)
      .replace(patterns.plain, emoticons.plain)
      .replace(patterns.grin, emoticons.grin)
      .replace(patterns.surprise, emoticons.surprise)
      .replace(patterns.dazed, emoticons.dazed);

    text = text.replace(regExpImageLinkOnly, "<img src=\"" + "$1$2" + "\"style=\"max-height: 250px\"/>")
      .replace(regExpVideoLinkOnly, "<video controls autoplay loop style=\"max-height: 250px\"><source src=\"" +"$1$2" + "\">Dein Browser ist zu unfähig dieses Video anzuzeigen!</video>")
      .replace(regExpYoutubeEmbedLinkOnly,"<iframe width=\"560\" height=\"315\" src=\"$1$2$3$4$5$6\" frameborder=\"0\" allowfullscreen></iframe>")
      .replace(regExpYoutubeWatchLinkOnly,"<iframe width=\"560\" height=\"315\" src=\"$1$2$3embed/$6\" frameborder=\"0\" allowfullscreen></iframe>")
      .replace(regExpYoutubeShareLinkOnly,"<iframe width=\"560\" height=\"315\" src=\"https://youtube.com/embed/$4\" frameborder=\"0\" allowfullscreen></iframe>")
      .replace(regExpLinkOnly, "<a href=\"$1\" target=\"_blank\" style=\"color: #58a5f1 !important\">$1</a>");

    return text;
  }
}
