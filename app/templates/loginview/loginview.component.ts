import { Component, ViewChild} from '@angular/core';
import {LoginComponent} from "../login/login.component";
import {ChatRoomListComponent} from "../chatroomlist/chatroomlist.component";

@Component({
  selector: 'loginview-component',
  templateUrl: "app/templates/loginview/loginview.html",
  directives: [LoginComponent, ChatRoomListComponent]
})
export class LoginViewComponent {
  @ViewChild(LoginComponent)
  private loginComponent: LoginComponent;

  private joinChatRoom(chatRoom: string): void {
    this.loginComponent.joinChatRoom(chatRoom);
  }
}
