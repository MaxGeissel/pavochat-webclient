import {Component, OnInit, Output, EventEmitter} from "@angular/core";
import {ChatServerConnection} from "../chatserverconnection/chatserverconnection";

@Component({
    selector: "chatroomlist-component",
    styleUrls: ["app/styles/chatroomlist/chatroomlist.css"],
    templateUrl: "app/templates/chatroomlist/chatroomlist.html",
})
export class ChatRoomListComponent implements OnInit {
  private _chatRooms: Array<string>;
  private _chatService: ChatServerConnection;
  @Output() onItemClicked = new EventEmitter<string>();

  constructor(chatService: ChatServerConnection) {
    this._chatService = chatService;
    this._chatRooms = [];
  }

  ngOnInit () {
    this._chatService.updateChatRooms();
    this._chatService.getChatRoomsObservable().subscribe((item: any) => {
      this._chatRooms = item;
    });
  }

  private joinChatRoom(chatRoom: string): void {
    this.onItemClicked.emit(chatRoom);
  }
}
