import {Component, AfterViewChecked, ElementRef, ViewChild, Input, OnInit } from "@angular/core";
import {FORM_DIRECTIVES, CORE_DIRECTIVES} from "@angular/common";
import {Message} from "../Message";
import {ChatServerConnection} from "../chatserverconnection/chatserverconnection";
import {MediaParserService} from "../../services/mediaparser/mediaparser.service";

@Component({
    selector: 'chat-component',
    templateUrl: "app/templates/chat/chat.html",
    styleUrls: ["app/styles/chat/chat.css"],
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
    providers: [MediaParserService]
})
export class ChatComponent implements AfterViewChecked, OnInit  {
  @Input() _userName: string;
  @Input() _chatRoom: string;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  private _chatService: ChatServerConnection;
  private _mediaParserService: MediaParserService;
  private _messages: Array<Message>;
  private _newMessageText: string;

  constructor(
    chatCommunicationService: ChatServerConnection,
    mediaParserService: MediaParserService) {
    this._chatService = chatCommunicationService;
    this._mediaParserService = mediaParserService;
    this._messages = [];
  }

  ngOnInit () {
    this._chatService.joinChatRoom(this._userName, this._chatRoom);
    this._chatService.getNewMessageObservable().subscribe((msg: Message) =>
      this._messages.push(
        new Message(
          msg.userName,
          this._mediaParserService.resolveLinksAndMedia(msg.text),
          msg.date)));
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  private scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { }
  }

  private send(): void {
    this._chatService.sendMessageToChatRoom(new Message(this._userName, this._newMessageText, new Date()));
    this._newMessageText = "";
  }
}
