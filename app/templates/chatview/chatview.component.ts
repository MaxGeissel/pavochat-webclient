import { Component } from '@angular/core';
import {ChatComponent} from '../chat/chat.component';
import {UserListComponent} from '../userlist/userlist.component';
import {RouteParams, Router} from "@angular/router-deprecated";

function isLengthValid(input: string): boolean {
  return (input.trim().length > 0 && input.trim().length < 30);
}

@Component({
  templateUrl: "app/templates/chatview/chatview.html",
  directives: [ChatComponent, UserListComponent]
})
export class ChatViewComponent {
  private _userName: string;
  private _chatRoom: string;

  constructor(routeParams: RouteParams, private _router: Router) {
    this._userName = routeParams.get('userName');
    this._chatRoom = routeParams.get('chatRoom');
    if (!isLengthValid(this._userName) || !isLengthValid(this._chatRoom)) {
      this._router.navigate(['Login',{}]);
    }
  }
}
