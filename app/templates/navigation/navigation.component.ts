import {Component} from "@angular/core";
import { ROUTER_DIRECTIVES, Router } from "@angular/router-deprecated";
import {ChatServerConnection} from "../chatserverconnection/chatserverconnection";


@Component({
    selector: "navigation-component",
    templateUrl: "app/templates/navigation/navigation.html",
    styleUrls: ["app/styles/navigation/navigation.css"],
    directives: [ROUTER_DIRECTIVES]
})
export class NavigationComponent {
  private _showLiveStream: boolean;
  constructor(
    private _router: Router,
    private _chatServer: ChatServerConnection
  ) {}

  private showLiveStream(): void {
    if (this._showLiveStream) {
      this._showLiveStream = false;
    }
    else {
      this._showLiveStream = true;
    }
  }

  private navigateToHome(): void {
    this._chatServer.leaveChatRoom();
    this._router.navigate(['Login',{}]);
  }
}
