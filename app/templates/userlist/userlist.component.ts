import {Component, OnInit} from "@angular/core";
import {ChatServerConnection} from "../chatserverconnection/chatserverconnection";

@Component({
    selector: "userlist-component",
    styleUrls: ["app/styles/userlist/userlist.css"],
    templateUrl: "app/templates/userlist/userlist.html",
})
export class UserListComponent implements OnInit {
  private _userList: Array<string>;
  private _chatService: ChatServerConnection;

  constructor(chatService: ChatServerConnection) {
    this._chatService = chatService;
  }

  ngOnInit () {
    this._chatService.getUserListObservable().subscribe((item: Array<string>) => {
      this._userList = item;
    });
  }
}
