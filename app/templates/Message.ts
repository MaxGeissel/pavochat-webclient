export class Message {
  public userName: string;
  public text: string;
  public date: Date;
  constructor(username: string, text: string, date: Date) {
    this.userName = username;
    this.text = text;
    this.date = date;
  }
}
