import {Component} from "@angular/core";
import { Router } from "@angular/router-deprecated";
import {NgClass} from "@angular/common";

function isLengthValid(input: string): boolean {
  return (input.trim().length > 0 && input.trim().length < 30);
}

@Component({
    selector: 'login-component',
    templateUrl: "app/templates/login/login.html",
    styleUrls: ["app/styles/login/login.css"],
    directives: [NgClass]
})
export class LoginComponent {
  private _router: Router;
  private _userName: string;
  private _chatRoom: string;

  constructor(router: Router) {
    this._router = router;
    this._userName = "";
    this._chatRoom = "";
  }

  public joinChatRoom(chatRoom: string): void {
    this._chatRoom = chatRoom;
    if (this.isUserInputValid()) {
      this._router.navigate(['ChatView',{userName: this._userName, chatRoom: this._chatRoom}]);
    }
  }

  private isUserInputValid(): boolean {
    return isLengthValid(this._userName) && isLengthValid(this._chatRoom);
  }
}
