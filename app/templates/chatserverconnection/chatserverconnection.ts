import { Injectable } from '@angular/core';
import {Message} from '../../templates/Message';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

declare var io: any;
const serverUrl: string =  "http://10.2.179.198:3001";//

@Injectable()
export class ChatServerConnection {
    private _socket: any;
    private _newMessage$: Subject<Message>;
    private _userList$: Subject<Array<string>>;
    private _chatRooms$: Subject<Array<string>>;
    private _errors$: Subject<number>;

    constructor() {
      this._socket = io(serverUrl);
      this._newMessage$ = <Subject<Message>>new Subject();
      this._userList$ = <Subject<Array<string>>>new Subject();
      this._chatRooms$ = <Subject<Array<string>>>new Subject();
      this._errors$ = <Subject<number>>new Subject();

      this._socket.on("updateChat", (userName: string, text: string, date: string) => {
        this._newMessage$.next(new Message(userName, text, new Date(date)));
      });
      this._socket.on("updateUsers", (userNames: Array<string>) => {
        this._userList$.next(userNames);
      });
      this._socket.on("updateChatRooms", (chatRooms: Array<string>) => {
        this._chatRooms$.next(chatRooms);
      });
      this._socket.on("error", (errorId: number) => {
        this._errors$.next(errorId);
      });
    }

    public joinChatRoom(userName: string, chatRoom: string): void {
      this._socket.emit("joinChatRoom", {"userName": userName, "chatRoom": chatRoom});
    }

    public leaveChatRoom(): void {
      this._socket.emit("leaveRoom");
    }

    public sendMessageToChatRoom(message: Message): void {
      this._socket.emit("sendMessage", {message: message});
    }

    public getNewMessageObservable(): Observable<Message> {
      return this._newMessage$.asObservable();
    }

    public getUserListObservable(): Observable<Array<string>> {
      return this._userList$.asObservable();
    }

    public getChatRoomsObservable(): Observable<Array<string>> {
      return this._chatRooms$.asObservable();
    }

    public updateChatRooms(): void {
      this._socket.emit("getUpdatedChatRooms");
    }

    public getErrorMessagesObservable(): Observable<number> {
      return this._errors$.asObservable();
    }
}
